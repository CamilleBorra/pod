<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap.min.css" type="text/css">
    <title>Ajouter</title>
</head>
<body>
    <div class="container">
        <form method="POST" action=""> 
        <?php include 'insertContenu.php';?>
            <div class="form-group">
                <label class="col-form-label mt-4" for="id">ID</label>
                <input type="number" class="form-control"  id="id" name="id">
            </div>

            <div class="form-group">
                <label class="col-form-label mt-4" for="duree">Duree</label>
                <input type="number" class="form-control"  id="duree" name="duree">
            </div>

            <div class="form-group">
                <label class="col-form-label mt-4" for="sot">share of time</label>
                <input type="number" class="form-control"  id="sot" name="sot">
            </div>

            <fieldset class="form-group">
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="pubVille" id="pubVille" value="pub" checked="">
                        Pub
                    </label>
                    
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="pubVille" id="pubVille" value="ville">
                        Ville
                    </label>
                </div>

               
            </fieldset>

            <div class="form-group">
                        <label class="col-form-label mt-4" for="couleur">Couleur</label>
                        <input data-jscolor="{}" value="#3399FF" name="couleur" id="couleur">
            </div>
            
            <button type="submit" name="submit" class="btn btn-primary">Inserer</button>

        </form>
    </div>
    <script src="jscolor.js"></script>
</body>
</html>