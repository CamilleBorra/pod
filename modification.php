<?php require_once "bdd.php"; 
$id=$_GET['id'];
$sql=$db->query("SELECT idContenu, duree, pubVille, couleur FROM contenu WHERE idContenu=\"$id\" ")
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap.min.css" type="text/css">
    <title>Modification</title>
</head>
<body>
<table class="table table-hover">
        
        <thead>
            <tr>
                
                <td>Id</td>
                <td>Durée</td>
                <td>pub/ville</td>
            </tr>
        </thead>

        <?php while($sqlExe=$sql->fetch()):?>
        <tbody>
            
           
         
            <tr style="background-color: <?=$sqlExe['couleur']?>;">
            
               
                <td><?= $sqlExe['idContenu']; ?> </td>
                <td><?= $sqlExe['duree']?> </td>
                <td><?= $sqlExe['pubVille']?> </td>
                

            </tr>
        </tbody>
        <form method="POST" action="update.php?id=<?=$id?>">
            <div class="form-group">
                    <label class="col-form-label mt-4" for="color">Nouvelle Couleur</label>
                    <input type="text" class="form-control"  id="color" name="color">
            </div>

            <button type="submit" name="submit" class="btn btn-primary">Modifier</button>
        </form>
        
        <form method="POST" action="delete.php?id=<?=$id?>">
         

            <button type="submit" name="submit" class="btn btn-primary">Supprimer le contenu</button>
        </form>
        
        
        <?php endwhile;?>
    </table>
</body>
</html>