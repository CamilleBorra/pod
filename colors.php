<?php 
    require_once "bdd.php";

    $recupContenu = $db->query("SELECT idContenu, pubVille, couleur FROM contenu");

    if($recupContenu->rowCount() !== 0) {
        $recupContenuExe = $recupContenu->fetchAll(PDO::FETCH_ASSOC);

        for ($i = 0; $i < count($recupContenuExe); $i++) {
            $element = $recupContenuExe[$i];

            echo $element['idContenu'] . ' - ' . $element['pubVille'] . ' - ' . $element['couleur'] . ' - <div style="height: 20px; width: 20px; display:inline-block; background-color: ' . $element['couleur'] . ';"></div><br />';
        }
    }

?>