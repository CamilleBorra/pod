
<?php
date_default_timezone_set('Europe/Paris');
require_once 'bdd.php';

if(isset($_POST['submit']))
{
   //RECUPERATION DES DONNEE DU FORMULAIRE
    $dateDebut=$_POST['dateDebut'];
    $dateFin=$_POST['dateFin'];
    $heureDebut=$_POST['heureDebut'];
    $heureFin=$_POST['heureFin'];
    $dateDebut=$dateDebut.' '.$heureDebut;
    $dateFin=$dateFin.' '.$heureFin;
    $objectDateDebut=new DateTime($dateDebut);
    $objectDateFin=new DateTime($dateFin);
    //$dateDebut=strval($dateDebut);
    //$dateFin=strval($dateFin);
    
    $vDGBO=$_POST['vDGBO'];
    $vDGBR=$_POST['vDGBR'];
    $vDGPA=$_POST['vDGPA'];
    $player=$_POST['modKis'];

    $conclusion=$_POST['conclusion'];
    $idTest=$_POST['idTest'];
    $timestamp=time();
    
    $idTest=$idTest.'_'.$timestamp;
    $idPod=$_POST['pod'];

    $idContenu1=$_POST['contenu1'];
    $idContenu2=$_POST['contenu2'];
    $idContenu3=$_POST['contenu3'];
    $idContenu4=$_POST['contenu4'];
    $idContenu5=$_POST['contenu5'];
    $idContenu6=$_POST['contenu6'];
    $idContenu7=$_POST['contenu7'];
    $idContenu8=$_POST['contenu8'];
    $idContenu9=$_POST['contenu9'];
    $idContenu10=$_POST['contenu10'];
    $idContenu11=$_POST['contenu11'];
    $idContenu12=$_POST['contenu12'];
    $idContenu13=$_POST['contenu13'];
    $idContenu14=$_POST['contenu14'];
    $idContenu15=$_POST['contenu15'];
    $idContenu16=$_POST['contenu16'];
    $idContenu17=$_POST['contenu17'];
    $idContenu18=$_POST['contenu18'];
    $idContenu19=$_POST['contenu19'];
    $idContenu20=$_POST['contenu20'];
   /* 
    $esp1=$_POST['esp1'];
    $esp2=$_POST['esp2'];
    $esp3=$_POST['esp3'];
    $esp4=$_POST['esp4'];
    $esp5=$_POST['esp5'];
    $esp6=$_POST['esp6'];
    $esp7=$_POST['esp7'];
    $esp8=$_POST['esp8'];
    $esp9=$_POST['esp9'];
    $esp10=$_POST['esp10'];
    */
    $tabCompteur=[
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
        [0,$dateDebut,0],
      
    ];
    
   
   // $depart=$_POST['depart'];
    //$fin=$_POST['fin'];

    $contenu=$_POST['contenu'];
    
    $tabContenu=[$idContenu1,$idContenu2,$idContenu3,$idContenu4,$idContenu5,$idContenu6,$idContenu7,$idContenu8,$idContenu9,$idContenu10,$idContenu11,$idContenu12,$idContenu13,$idContenu14,$idContenu15,$idContenu16,$idContenu17,$idContenu18,$idContenu19,$idContenu20];
    //Recuperation de la table composition
    $recupCompo=$db->query("SELECT com.idContenu,dateAt,heure,duree,share,idUnique,pubVille,couleur From composition com,contenu con WHERE (com.idContenu=\"$idContenu1\" OR com.idContenu=\"$idContenu2\" OR com.idContenu=\"$idContenu3\" OR com.idContenu=\"$idContenu4\" OR com.idContenu=\"$idContenu5\" OR com.idContenu=\"$idContenu6\"  OR com.idContenu=\"$idContenu7\" OR com.idContenu=\"$idContenu8\" OR com.idContenu=\"$idContenu9\" OR com.idContenu=\"$idContenu10\" OR com.idContenu=\"$idContenu11\" OR com.idContenu=\"$idContenu12\" OR com.idContenu=\"$idContenu13\" OR com.idContenu=\"$idContenu14\" OR com.idContenu=\"$idContenu15\" OR com.idContenu=\"$idContenu16\" OR com.idContenu=\"$idContenu17\" OR com.idContenu=\"$idContenu18\" OR com.idContenu=\"$idContenu19\" OR com.idContenu=\"$idContenu20\") AND idPOD=$idPod AND com.idContenu=con.idContenu AND dateAt>=\"$dateDebut\" AND dateAt<=\"$dateFin\" ORDER BY idUnique");
  
    $recupFrequenceCompo=$db->query("SELECT COUNT( com.idContenu ) AS frequence,com.idContenu,dateAt,heure,duree,share,idUnique,pubVille,couleur From composition com,contenu con WHERE (com.idContenu=\"$idContenu1\" OR com.idContenu=\"$idContenu2\" OR com.idContenu=\"$idContenu3\" OR com.idContenu=\"$idContenu4\" OR com.idContenu=\"$idContenu5\" OR com.idContenu=\"$idContenu6\"  OR com.idContenu=\"$idContenu7\" OR com.idContenu=\"$idContenu8\" OR com.idContenu=\"$idContenu9\" OR com.idContenu=\"$idContenu10\" OR com.idContenu=\"$idContenu11\" OR com.idContenu=\"$idContenu12\" OR com.idContenu=\"$idContenu13\" OR com.idContenu=\"$idContenu14\" OR com.idContenu=\"$idContenu15\" OR com.idContenu=\"$idContenu16\" OR com.idContenu=\"$idContenu17\" OR com.idContenu=\"$idContenu18\" OR com.idContenu=\"$idContenu19\" OR com.idContenu=\"$idContenu20\") AND idPOD=$idPod AND com.idContenu=con.idContenu AND dateAt>=\"$dateDebut\" AND dateAt<=\"$dateFin\" GROUP BY idContenu ");
    /*while($recupFrequenceCompoExe=$recupFrequenceCompo->fetch())
    {var_dump($recupFrequenceCompoExe['frequence']);}*/
   
    // recuperation du contenu et insertion des tests
    $recuCon=$db->query("SELECT * FROM contenu WHERE idContenu=\"$idContenu1\" OR idContenu=\"$idContenu2\" OR idContenu=\"$idContenu3\" OR idContenu=\"$idContenu4\" OR idContenu=\"$idContenu5\" OR idContenu=\"$idContenu6\" OR idContenu=\"$idContenu7\" OR idContenu=\"$idContenu8\" OR idContenu=\"$idContenu9\" OR idContenu=\"$idContenu10\" OR idContenu=\"$idContenu11\" OR idContenu=\"$idContenu12\" OR idContenu=\"$idContenu13\" OR idContenu=\"$idContenu14\" OR idContenu=\"$idContenu15\" OR idContenu=\"$idContenu16\" OR idContenu=\"$idContenu17\" OR idContenu=\"$idContenu18\" OR idContenu=\"$idContenu19\" OR idContenu=\"$idContenu20\"");
    

    $insertTest=$db->prepare("INSERT INTO testunitaire (idTest,dateDebut,dateFin,versionDigiBoard,versionDigiBrain,versionDigiPartner,conclusion) VALUES (:idTest,:dateDebut,:dateFin,:versionDigiBoard,:versionDigiBrain,:versionDigiPartner,:conclusion)");
    $insertTest -> execute ([
        'idTest'=>$idTest,
        'dateDebut'=>$dateDebut,
        'dateFin'=>$dateFin,
        'versionDigiBoard'=>$vDGBO,
        'versionDigiBrain'=>$vDGBR,
        'versionDigiPartner'=>$vDGPA,
        'conclusion'=>$conclusion

    ]);

    //UPLOAD FICHIER JSON DE LA PLAYLIST
 /*  $files = glob('fichierJson/*'); // get all file names
    foreach($files as $file){ // iterate files
    if(is_file($file))
    unlink($file); // delete file
    }

   $target_dir = "fichierJson/";
    $namePicture=basename("resource.json");
   

    $target_file = $target_dir.$namePicture ;
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image

    move_uploaded_file($_FILES["jsonFile"]["tmp_name"], $target_file);
    */
        //création du Json pour les pourcentages espéré
     /*   $filename="jsonInfo.json";
        if(file_exists($filename))
        {
            unlink($filename);
        }
        $tabEsp=[   
                $esp1,
                $esp2,
                $esp3,
                $esp4,
                $esp5,
                 $esp6,
                 $esp7,
                 $esp8,
                 $esp9,
                 $esp10
                ];
        $tabContenu=[
            $idContenu1,
            $idContenu2,
            $idContenu3,
            $idContenu4,
            $idContenu5,
            $idContenu6,
            $idContenu7,
            $idContenu8,
            $idContenu9,
            $idContenu10
        ];
        $tabCoco=[];
        for( $i=0;$i<10;$i++)
        {
            
            $recupColor=$db->query("SELECT couleur FROM contenu Where idContenu=\"$tabContenu[$i]\"");
            
            $recupColorExe=$recupColor->fetch();
            array_push($tabCoco,$recupColorExe['couleur']);

        }

        $myJson=[
            'tabEsp'=>$tabEsp,
            'tabContenu'=>$tabContenu,
            'tabCouleur'=>$tabCoco
        
            
        ];
        $myJson=json_encode($myJson);
        $nom_file = $filename;
        $texte =$myJson;
        // création du fichier
        $f = fopen($nom_file, "x+");
        // écriture
        fputs($f, $texte );
        // fermeture
        fclose($f);
*/
        // Création du json avec le nom des animations et le nombre de repetition
        $tabFrequence=[];
        $tabContenuRecup=[];
        $tabDuree=[];
        $tabCouleur=[];
       
        $recupFrequence=$db->query("SELECT nbRepetition,idContenu,duree,couleur FROM contenu WHERE idContenu=\"$idContenu1\" OR idContenu=\"$idContenu2\" OR idContenu=\"$idContenu3\" OR idContenu=\"$idContenu4\" OR idContenu=\"$idContenu5\" OR idContenu=\"$idContenu6\" OR idContenu=\"$idContenu7\"  OR idContenu=\"$idContenu8\"  OR idContenu=\"$idContenu9\"  OR idContenu=\"$idContenu10\" OR idContenu=\"$idContenu11\" OR idContenu=\"$idContenu12\" OR idContenu=\"$idContenu13\" OR idContenu=\"$idContenu14\" OR idContenu=\"$idContenu15\" OR idContenu=\"$idContenu16\" OR idContenu=\"$idContenu17\" OR idContenu=\"$idContenu18\" OR idContenu=\"$idContenu19\" OR idContenu=\"$idContenu20\"");
        $recupFrequenceCompoExe=$recupFrequenceCompo->fetchAll();
        for($i=0;$i<count($recupFrequenceCompoExe);$i++)
        {
            array_push($tabFrequence,$recupFrequenceCompoExe[$i]['frequence']);
            array_push($tabContenuRecup,$recupFrequenceCompoExe[$i]['idContenu']);
            array_push($tabDuree,$recupFrequenceCompoExe[$i]['duree']);
            array_push($tabCouleur,$recupFrequenceCompoExe[$i]['couleur']);
        }
        $recupFrequenceCompo->closeCursor();
        $filename2="jsonInfo2.json";
        if(file_exists($filename2))
        {
            unlink($filename2);
        }
        
        $myJson2=[
            'tabFrequence'=>$tabFrequence,
            'tabContenuRecup'=>$tabContenuRecup,
            'tabDuree'=>$tabDuree,
            'tabCouleur'=>$tabCouleur
        ];
        $myJson2=json_encode($myJson2);
        $nom_file2 = $filename2;
        $texte2 =$myJson2;
        // création du fichier
        $f2 = fopen($nom_file2, "x+");
        // écriture
        fputs($f2, $texte2 );
        // fermeture
        fclose($f2);
        $timePrecedent=new DateTime('00:00:00');
        while($recupCompoExe=$recupCompo->fetch()){
            $dateEnd=new DateTime($recupCompoExe['dateAt']);
               
            
                    
                    
            $delta=date_diff($dateEnd,$timePrecedent);
          
                    
            $deltas=$delta->format("%H:%I:%S");
           
            for($i=0;$i<count($tabContenu);$i++)
            {
                
                if($recupCompoExe['idContenu']==$tabContenu[$i])
                {
                   
                    
                    $dateStart=new DateTime($tabCompteur[$i][1]);
                    $timer=date_diff($dateStart, $dateEnd);
                    $timer=$timer->format("%H:%I:%S");
                    //var_dump($timer);
                    $insertResult=$db->prepare('INSERT INTO resultat (color,idContenu,duree,dateAt,nbSpot,timer,pubVille,idTest,delta,num) VALUES (:color,:idContenu,:duree,:dateAt,:nbSpot,:timer,:pubVille,:idTest,:delta,:num)');
                   
                    $insertResult->execute([
                        'color' => $recupCompoExe['couleur'],
                        'idContenu' => $recupCompoExe['idContenu'],
                        'duree' =>$recupCompoExe['duree'],
                        'dateAt'=> $recupCompoExe['dateAt'],
                        'nbSpot' =>$tabCompteur[$i][0],
                        'timer'=>$timer,
                        'pubVille'=>$recupCompoExe['pubVille'],
                        'idTest'=>$idTest,
                        'delta'=>$deltas,
                        'num' => $tabCompteur[$i][2]


                    ]);

                    $timePrecedent=$dateEnd;
                    for($j=0;$j<20;$j++){
                        if($j!=$i)
                        $tabCompteur[$j][0]++;

                        else
                        {
                            
                        $tabCompteur[$j][0]=0;
                        $tabCompteur[$j][1]=$recupCompoExe['dateAt'];
                        $tabCompteur[$j][2]++;
                        //var_dump($tabCompteur[$j][1]);
                    }

                        
                        
                    }

                    
                   
                }
             
            }
        }
        $recupResultat=$db->query("SELECT * FROM resultat WHERE idTest=\"$idTest\"");
        function moyenne ($id,$idTest,$db){
            $moyenne=$db->query("SELECT MAX(timer) as maxTimer, MIN(timer) as minTimer, MAX(nbSpot) as maxSpot, MIN(nbSpot) as minSpot, ROUND(AVG(REPLACE(timer, ':', '')), 2) as moyTimer, AVG(nbSpot) as moySpot FROM resultat WHERE idTest=\"$idTest\" AND idContenu=\"$id\" AND num>1");
            
            while($moyenneExe=$moyenne->fetch()){
                
                $tabMoyenne=['maxTimer'=>$moyenneExe['maxTimer'],'minTimer'=>$moyenneExe['minTimer'], 'maxSpot'=>$moyenneExe['maxSpot'], 'minSpot'=>$moyenneExe['minSpot'], 'moySpot'=>$moyenneExe['moySpot'], 'moyTimer'=>$moyenneExe['moyTimer']];
                return $tabMoyenne;
            }
        }

        
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap.min.css" type="text/css">
    <title>Recette KIS</title>
</head>

<body>
    <style>
        @media print {
        tr{
            -webkit-print-color-adjust: exact;
            }
        }
    </style>
  
    <div class="container">
    <h2>Mise en place:</h2>
    <p>Lien pour récupérer les données : https://live-doae.jcdecaux.com/pod/export.php?test=<?= $idTest?></p>
    <p>Test : <?= $idTest; ?></p>
    <p>Contenu: <?= nl2br($contenu); ?></p>
    <p>Date de réalisation: <?= $dateDebut." / ".$dateFin?></p>
    <p>Version digiboard: <?=$vDGBO?></p>
    <p>Version digibrain: <?=$vDGBR?></p>
    <p>Version digipartner: <?=$vDGPA?></p>
    <p>Modèle KIS: <?=$player?></p>
    <table class="table table-hover">


        <thead>
            <tr>
                
                <td>Id</td>
                <td>Nombre d'occurence</td>
                <td>Min Timer</td>
                <td>Max Timer</td>
                
                <td>Min nbSpot</td>
                <td>Max nbSpot</td>
                <td>Moyenne nbSpot</td>
            </tr>
        </thead>
        <?php for($i=0;$i<count($recupFrequenceCompoExe);$i++):?>
        <tbody>
            <tr>
                <?php $recupMoyenne=moyenne($recupFrequenceCompoExe[$i]['idContenu'],$idTest,$db);?>
                <?php $croix=($recupMoyenne['moyTimer']*60)/100;?>
                <td><?= $recupFrequenceCompoExe[$i]['idContenu'];?></td>
                <td><?= $recupFrequenceCompoExe[$i]['frequence']?></td>
                <td><?= $recupMoyenne['minTimer']?></td>
                <td><?= $recupMoyenne['maxTimer']?></td>
                
                <td><?= $recupMoyenne['minSpot']?></td>
                <td><?= $recupMoyenne['maxSpot']?></td>
                <td><?= $recupMoyenne['moySpot']?></td>
            </tr>
        </tbody>
        <?php endfor?>

    </table>

    <h2>Répartition des contenus:</h2>
   <!-- <h3>Objectif:</h3>
    <canvas id="pieChartObj" width="300px" ></canvas>
    <br> -->
    <!-- <h3>D'après la playlist:</h3>
    <canvas id="pieChartPlay" width="300px"></canvas>
    <br> -->
    <h3>Réalité:</h3>
    <canvas id="pieChartReal" height="900px"></canvas>
    <br>

    <h2>Ordre de passage:</h2>
    <!--
        <h3>Objectif:</h3>
    <canvas id="lineChartObj" width="100%"></canvas>
    <br>
     <h3>D'après la playlist:</h3>
    <table>
        <tr id=td1> </tr>
        <tr id=td2> </tr>
        <tr id=td3> </tr>
        <tr id=td4> </tr>
        <tr id=td5> </tr>
        <tr id=td6> </tr>
        <tr id=td7> </tr>
        <tr id=td8> </tr>
        <tr id=td9> </tr>
        <tr id=td10> </tr>
    </table> -->
    <br>
    <h3>Réalité:</h3>
    <table class="table table-hover">
        
        <thead>
            <tr>
                
                <td>Id</td>
                <td>Durée</td>
                <td>Date</td>
                <td>NbSpot</td>
                <td>Timer</td>
                <td>pub/ville</td>
            </tr>
        </thead>

        <?php while($recupResultatExe=$recupResultat->fetch()):?>
        <tbody>
            
           
         
            <tr style="background-color: <?=$recupResultatExe['color']?>;">
            
               
                <td><?= $recupResultatExe['idContenu']; ?> </td>
                <td><?= $recupResultatExe['duree']?> </td>
                <td><?= $recupResultatExe['dateAt']?> </td>
                <td><?= $recupResultatExe['nbSpot']?> </td>
                <td><?= $recupResultatExe['timer']?> </td>
                <td><?= $recupResultatExe['pubVille']?> </td>
            </tr>
        </tbody>
        
        <?php endwhile;?>
    </table>
    <br>


    <h2>Conclusion:</h2>
    <?= $conclusion ?>

    </div>
</body>

</html>

<script src="jquery.js"></script>
<script src="chart.min.js"></script>
<script>
    //lecture de la playlist
 /*   $.ajax({
        url: "fichierJson/resource.json",
        method: 'GET',
        success: function (event) {

            //const fileJson = JSON.parse(event);
            console.log("success")
            //console.log(event);

            var sequence = event['playlist']['days'][0][0]['sequence']
            var contenu = event['playlist']['mediaVariants']
            // remplissage du tableau pour le nombre d'occurence

            var exist = 0;
            var tabData = [
                []
            ];
            var cpt = 1
            tabData[0][0] = sequence[0];
            tabData[0][1] = 1;
            for (var i = 1; i < sequence.length; i++) {
                tabData[i] = [];
                for (var j = 0; j < tabData.length; j++) {
                    const equals = (a, b) => JSON.stringify(a) === JSON.stringify(b);
                    const a = tabData[j][0];
                    const b = sequence[i];

                    //console.log("contenu".contenu)
                    const c = contenu[i]['reservationId'];
                     console.log (c);
                     const equals2 = (b, c) => JSON.stringify(b) === JSON.stringify(c);


                    if (equals(a, b)) {

                        exist = 1;
                        tabData[j][1] += 1;
                    }
                }
                if (exist == 0) {



                    tabData[cpt][0] = sequence[i];
                    tabData[cpt][1] = 1;
                    cpt = cpt + 1;
                } else {
                    exist = 0;
                }
                //console.log(tabData)
            }

            var tabDatas = []
            for (var i = 0; i < tabData.length; i++) {
                tabDatas[i] = tabData[i][1];
            }
            var data = {


                datasets: [{
                    label: 'Dataset 1',
                    data: tabDatas,
                    backgroundColor: ['#CB4335', '#1F618D', '#FB5676'],
                }]
            };

            new Chart(document.getElementById("pieChartPlay"), {
                type: 'pie',
                data: data,
                options: {
                    responsive: false,
                    plugins: {
                        legend: {
                            position: 'top',
                        },

                    }
                },
            });
            //c'est deguelasse voir comment ajouter des lignes automatiquement
            var longeur = sequence.length
            var j = 0
            document.getElementById('td1').innerHTML = sequence[j]
            console.log(sequence[j])
            j++
            document.getElementById('td2').innerHTML = sequence[j]
            j++
            if (j > longeur) {
                j = 0
            }
            document.getElementById('td3').innerHTML = sequence[j]
            j++
            if (j > longeur) {
                j = 0
            }
            document.getElementById('td4').innerHTML = sequence[j]
            j++
            if (j > longeur) {
                j = 0
            }
            document.getElementById('td5').innerHTML = sequence[j]
            j++
            if (j > longeur) {
                j = 0
            }
            document.getElementById('td6').innerHTML = sequence[j]
            j++
            if (j > longeur) {
                j = 0
            }
            document.getElementById('td7').innerHTML = sequence[j]
            j++
            if (j > longeur) {
                j = 0
            }
            document.getElementById('td8').innerHTML = sequence[j]
            j++
            if (j > longeur) {
                j = 0
            }
            document.getElementById('td9').innerHTML = sequence[j]
            j++
            if (j > longeur) {
                j = 0
            }
            document.getElementById('td10').innerHTML = sequence[j]
            j++
        },
        error: function (err) {
            console.error(err)
            console.log("error")
        }
    })*/
    // lecture des pourcentages espere
    /*
    $.ajax({
        url: "jsonInfo.json",
        method: 'GET',
        success: function (event) {
            var tabEsp = event['tabEsp']
            var tabContenu= event['tabContenu']
            var tabColor=event ['tabCouleur']
            console.log (tabEsp)
            console.log (tabContenu)
            console.log (tabColor)
            var data = {
                labels:tabContenu,

                datasets: [{
                    label: 'Dataset 1',
                    data: tabEsp,
                    backgroundColor: tabColor,
                }]
            };

            new Chart(document.getElementById("pieChartObj"), {
                type: 'pie',
                data: data,
                options: {
                    responsive: false,
                    

                    plugins: {
                        legend: {
                            position: 'top',
                        },

                    }
                },
            });
        },
        error: function (err) {
            console.error(err)
            console.log("error")
        }
    })*/
    // lecture des nombre de répétition
    $.ajax({
        url: "jsonInfo2.json",
        method: 'GET',
        success: function (event) {
            var nbRep = [];
            var somme=0;
            var pourcentage=[];
            var tabCouleur = event['tabCouleur'] 
            var contenuRecup= event['tabContenuRecup']
            for (var i = 0; i < event['tabDuree'].length; i++) {
                nbRep[i] = event['tabDuree'][i] * event['tabFrequence'][i]
            }
            for (var i = 0; i < nbRep.length; i++) {
                somme+=nbRep[i]
            }

            for(var i = 0; i < nbRep.length; i++){
                pourcentage[i]=(nbRep[i]/somme)*100

            }

            for(var i=0; i<contenuRecup.length;i++){
                contenuRecup[i]=contenuRecup[i]+' - '+pourcentage[i]+'%'
            }

            var data = {

                labels : contenuRecup,
                datasets: [{
                    label: 'Dataset 1',
                    data: nbRep,
                    backgroundColor: tabCouleur,
                }]
            };

            new Chart(document.getElementById("pieChartReal"), {
                type: 'pie',
                
                data: data,
                
                options: {
                    responsive: false,
                    plugins: {
                        legend: {
                            position: 'top',
                        },
                        datalabels: {
                         label: pourcentage,
                         },

                    }
                },
            });
        },
        error: function (err) {
            console.error(err)
            console.log("error")
        }
    })
</script>