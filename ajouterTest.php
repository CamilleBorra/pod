<?php require_once "affichage.php" ;?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap.min.css" type="text/css">
    <title>AjoutTest</title>
</head>
<body>
<div class="container">
    <form method="POST" action="insertTest.php" enctype="multipart/form-data"> 
        
       <!-- <div class="form-group">
            <label for="test" class="form-label mt-4">Test</label>
            <select class="form-select" id="test" name="test">
            <?php // while ($recupTestExe=$recupTest->fetch()): ?>
                
                <option value=<?php // echo $recupTestExe['idTest']?>><?// echo $recupTestExe['libelle']?></option>
            <?php // endwhile;?>
               
            </select>
        </div> -->

            <div class="form-group">
                <label class="col-form-label mt-4" for="idTest">test</label>
                <input type="text" class="form-control"  id="idTest" name="idTest">
            </div>
            <div class="form-group">
                <label for="contenu" class="form-label mt-4">Contenu:</label>

                <textarea class="form-control" id="contenu" name="contenu" rows="4" cols="50">
               
                </textarea>
            </div>

            <div class="form-group">
                <label class="col-form-label mt-4" for="dateDebut">Date de début</label>
                <input type="date" class="form-control"  id="dateDebut" name="dateDebut">
            </div>
            <div class="form-group">
                <label class="col-form-label mt-4" for="heureDebut">Heure de début</label>
                <input type="text" class="form-control"  id="heureDebut" name="heureDebut">
            </div>

            <div class="form-group">
                <label class="col-form-label mt-4" for="dateFin">Date de fin</label>
                <input type="date" class="form-control"  id="dateFin" name="dateFin">
            </div>

            <div class="form-group">
                <label class="col-form-label mt-4" for="heureFin">Heure de fin</label>
                <input type="text" class="form-control"  id="heureFin" name="heureFin">
            </div>

            <div class="form-group">
                <label class="col-form-label mt-4" for="vDGBO">Version de digiboard</label>
                <input type="text" class="form-control"  id="vDGBO" name="vDGBO">
            </div>

            <div class="form-group">
                <label class="col-form-label mt-4" for="vDGBR">Version de digibrain</label>
                <input type="text" class="form-control"  id="vDGBR" name="vDGBR">
            </div>

            <div class="form-group">
                <label class="col-form-label mt-4" for="vDGPA">Version de digipartner</label>
                <input type="text" class="form-control"  id="vDGPA" name="vDGPA">
            </div>

            <div class="form-group">
                <label class="col-form-label mt-4" for="modKis">Modèle de KIS</label>
                <input type="text" class="form-control"  id="modKis" name="modKis">
            </div>
            
            <div class="form-group">
                <label class="col-form-label mt-4" for="pod">Pod</label>
                <input type="text" class="form-control"  id="pod" name="pod">
            </div>
            

            <div class="form-group">
                <label for="contenu1" class="form-label mt-4">Contenu 1</label>
                <select class="form-select" id="contenu1" name="contenu1">
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>
            
            <!--
            <div class="form-group">
                <label class="col-form-label mt-4" for="esp1">Resultat éspéré</label>
                <input type="text" class="form-control"  id="esp1" name="esp1" value=0>
            </div>
                    -->

          

            <div class="form-group">
                <label for="contenu2" class="form-label mt-4">Contenu 2</label>
                <select class="form-select" id="contenu2" name="contenu2">
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>
            <!--
            <div class="form-group">
                <label class="col-form-label mt-4" for="SOT2">Resultat éspéré</label>
                <input type="text" class="form-control"  id="SOT2" name="SOT2" value=0>
            </div>
                    -->
            
            <!--
            <div class="form-group">
                <label class="col-form-label mt-4" for="esp2">Resultat éspéré</label>
                <input type="text" class="form-control"  id="esp2" name="esp2" value=0>
            </div>
                    -->
            
            

            <div class="form-group">
                <label for="contenu3" class="form-label mt-4">Contenu 3</label>
                <select class="form-select" id="contenu3" name="contenu3">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>
            <!--
            <div class="form-group">
                <label class="col-form-label mt-4" for="esp3">Resultat éspéré</label>
                <input type="text" class="form-control"  id="esp3" name="esp3" value=0>
            </div>
                    -->

            <div class="form-group">
                <label for="contenu4" class="form-label mt-4">Contenu 4</label>
                <select class="form-select" id="contenu4" name="contenu4">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>
            <!--
            <div class="form-group">
                <label class="col-form-label mt-4" for="esp4">Resultat éspéré</label>
                <input type="text" class="form-control"  id="esp4" name="esp4" value=0>
            </div>
                    -->
            <div class="form-group">
                <label for="contenu5" class="form-label mt-4">Contenu 5</label>
                <select class="form-select" id="contenu5" name="contenu5">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select> 
            </div>
            <!--
            <div class="form-group">
                <label class="col-form-label mt-4" for="esp5">Resultat éspéré</label>
                <input type="text" class="form-control"  id="esp5" name="esp5" value=0>
            </div>
                    -->
            <div class="form-group">
                <label for="contenu6" class="form-label mt-4">Contenu 6</label>
                <select class="form-select" id="contenu6" name="contenu6">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>

            <!--
            <div class="form-group">
                <label class="col-form-label mt-4" for="esp6">Resultat éspéré</label>
                <input type="text" class="form-control"  id="esp6" name="esp6" value=0>
            </div> -->

            <div class="form-group">
                <label for="contenu7" class="form-label mt-4">Contenu 7</label>
                <select class="form-select" id="contenu7" name="contenu7">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>

            <!--
            <div class="form-group">
                <label class="col-form-label mt-4" for="esp7">Resultat éspéré</label>
                <input type="text" class="form-control"  id="esp7" name="esp7" value=0>
            </div> -->

            <div class="form-group">
                <label for="contenu8" class="form-label mt-4">Contenu 8</label>
                <select class="form-select" id="contenu8" name="contenu8">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>

            <!--
            <div class="form-group">
                <label class="col-form-label mt-4" for="esp8">Resultat éspéré</label>
                <input type="text" class="form-control"  id="esp8" name="esp8" value=0>
            </div> -->

            <div class="form-group">
                <label for="contenu9" class="form-label mt-4">Contenu 9</label>
                <select class="form-select" id="contenu9" name="contenu9">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>

            <!--
            <div class="form-group">
                <label class="col-form-label mt-4" for="esp9">Resultat éspéré</label>
                <input type="text" class="form-control"  id="esp9" name="esp9" value=0>
            </div>
                    -->


            <div class="form-group">
                <label for="contenu10" class="form-label mt-4">Contenu 10</label>
                <select class="form-select" id="contenu10" name="contenu10">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>

            <div class="form-group">
                <label for="contenu11" class="form-label mt-4">Contenu 11</label>
                <select class="form-select" id="contenu11" name="contenu11">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>

            <div class="form-group">
                <label for="contenu12" class="form-label mt-4">Contenu 12</label>
                <select class="form-select" id="contenu12" name="contenu12">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>

            <div class="form-group">
                <label for="contenu13" class="form-label mt-4">Contenu 13</label>
                <select class="form-select" id="contenu13" name="contenu13">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>

            <div class="form-group">
                <label for="contenu14" class="form-label mt-4">Contenu 14</label>
                <select class="form-select" id="contenu14" name="contenu14">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>

            
            <div class="form-group">
                <label for="contenu15" class="form-label mt-4">Contenu 15</label>
                <select class="form-select" id="contenu15" name="contenu15">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>
            
            
            <div class="form-group">
                <label for="contenu16" class="form-label mt-4">Contenu 16</label>
                <select class="form-select" id="contenu16" name="contenu16">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>

            
            <div class="form-group">
                <label for="contenu17" class="form-label mt-4">Contenu 17</label>
                <select class="form-select" id="contenu17" name="contenu17">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>

            
            <div class="form-group">
                <label for="contenu18" class="form-label mt-4">Contenu 18</label>
                <select class="form-select" id="contenu18" name="contenu18">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>

            
            <div class="form-group">
                <label for="contenu19" class="form-label mt-4">Contenu 19</label>
                <select class="form-select" id="contenu19" name="contenu19">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>

            
            <div class="form-group">
                <label for="contenu20" class="form-label mt-4">Contenu 20</label>
                <select class="form-select" id="contenu20" name="contenu20">
                        <option value="0"></option>
                    <?php for ($i=0;$i<count($recupContenuExe);$i++): ?>
                        
                        <option value=<?php echo $recupContenuExe[$i]['idContenu']?>><?=$recupContenuExe[$i]['idContenu']?></option>
                    <?php endfor;?>
                
                </select>
            </div>
            


            <!--
            <div class="form-group">
                <label class="col-form-label mt-4" for="esp10">Resultat éspéré</label>
                <input type="text" class="form-control"  id="esp10" name="esp10" value=0>
            </div>
                    -->

            


            <div class="form-group">
                <label class="col-form-label mt-4" for="conclusion">conclusion</label>
                <textarea class="form-control"  id="conclusion" name="conclusion" rows="5" cols="50"></textarea>
            </div>
           <!-- 
            <div class="form-group">
                    <h2>Mettez votre fichier Json</h2>
                    <label for="jsonFile">Fichier:</label>
                    <input type="file" name="jsonFile" id="jsonFile">
                    <p><strong>Note:</strong> Seul le format .json est autorisé.</p>
            </div>
                    -->
            
            <button type="submit" name="submit" id="submit" class="btn btn-primary">Inserer</button>

        </form>
        
    </div>
</body>
</html>