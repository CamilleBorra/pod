<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <form action="insertContenu.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="id" class="form-label mt-4">Id</label>
                <input type="number" class="form-control" id="id" name="id" required>
            </div>

            <div class="form-group">
                <label for="duree" class="form-label mt-4">Duree (en s)</label>
                <input type="number" class="form-control" id="duree" name="duree" required>
            </div>

            <div class="form-group">
                <label for="resolution" class="form-label mt-4">Résolution</label>
               
                <select class="form-select" id="resolution" name="resolution">
                    <option value="1920*1080">1920*1080</option>
                    <option value="1080*1920">1080*1920</option>
                </select>
                
            </div>

            <div class="form-group">
                <label for="pubVille" class="form-label mt-4">Type</label>
                <select class="form-select" id="pubVille" name="pubVille">
                    <option value="pub">pub</option>
                    <option value="ville">ville</option>
                </select>
            </div>

            <div class="form-group">
                <label for="orientation" class="form-label mt-4">Orientation</label>
                <select class="form-select" id="orientation" name="orientation">
                    <option value="paysage">paysage</option>
                    <option value="portrait">portrait</option>
                </select>
            </div>
            <div class="form-group">
                <label class="col-form-label mt-4" for="couleur">Couleur</label>
                <input data-jscolor="{}" value="#3399FF" name="couleur" id="couleur">
            </div>

            <div id="form-camera" class="form-group ">
                <label for="images">Image</label>
                <input type="file" id="images" name="images"  capture>
            </div>
            <div class="form-group " id="photo">
                <?php $tabImage=scandir('img');
                for($i=2;$i<count($tabImage);$i++):?>
                <img class="img" name="img" height="100" style="display:inline-block" id="<?= $tabImage[$i]?>" src="img/<?= $tabImage[$i]?>" alt="" onclick="getId(this)">
                <?php endfor?>
            </div>
            <input type="text" name="imageName" id='imageName' value="">
            <button type="submit" class="btn btn-primary" name="submit">Generer</button>

          
        </form>

    </div>
</body>
</html>
<script src="jscolor.js"></script>
<script src="jquery.js"></script>
<script>
    /*$("img").click(function(){
        console.log('ok')
        //$('.imageName').value= $('img').src
        document.getElementById('imageName').value=$('.img').attr('src')
        
        //var im= $('#6738_3.jpg').src
        //console.log($('#6738_3.jpg').src)
        
    })*/
    /*function source(){
        console.log('ok')
       // document.getElementById('imageName').value=$('.img').attr('src')
    }*/
    function getId(monId)
{
id=monId.id;

var textId='#'+id
console.log(textId);
document.getElementById('imageName').value=id

}
   
</script>